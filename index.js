console.log("Hello World.");

// NUMBER 1 - 8


// Address
	

// Animal
	

	
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
	const getCube = 2 ** 3;
	
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
	console.log(`The cube of 2 is ${getCube}`);
// 5. Create a variable address with a value of an array containing details of an address.
	const fulladdress = ["258 Washington Ave NW,", "California 90011"];
// 6. Destructure the array and print out a message with the full address using Template Literals.
	const [streetName, stateZip] = fulladdress
	console.log(`I live at ${streetName} ${stateZip}.`);
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
	const animal = {
			name: "Lolong",
			type: "salwater crocodile.",
			weight: "1075",
			measurement: "20 ft. 3 in."
		};

	const{name, type, weight, measurement }= animal;

	console.log(`${name} was a ${type} He weighed at ${weight} kgs with a measurement of ${measurement}`);;
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
// 9. Create an array of numbers.
const numbers = [1,2,3,4,5]
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach((number) =>{
	console.log(`${number}`)
});
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduceNumber = numbers.reduce(function(x,y){
	const add = (x, y) => x + y;
	return x + y;
	})
console.log(reduceNumber);
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
// 13. Create/instantiate a new object from the class Dog and console log the object.
 let firstDog = new Dog("Frankie", 5, "Miniature Dachshund");
 console.log(firstDog);